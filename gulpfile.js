// Globals:
var gulp        = require('gulp'),
    gutil       = require('gulp-util'),
    sequence    = require('run-sequence'),
    sourcemaps  = require('gulp-sourcemaps'),
    sass        = require('gulp-sass'),
    filter      = require('gulp-filter'),
    express     = require('express'),
    browserSync = require('browser-sync'),
    minimist    = require('minimist'),
    imagemin    = require('gulp-imagemin'),
    eslint      = require('gulp-eslint')
    jspm        = require('jspm'),
    replace     = require('gulp-replace'),
    remove      = require('gulp-remove-code'),
    htmlmin     = require('gulp-minify-html');

// CLI Options:
var options     = minimist(process.argv),
    environment = options.environment || 'development',
    server      = express(),
    config      = require('./config/' + environment + '.json'),
    path        = {
                    'src' : './src',
                    'out' : config.files.out,
                    "entry" : config.files.entry
                  };

// Clean output folder
gulp.task('clean', require('del').bind( null, path.out ));

// Sass
gulp.task('sass', function () {
  return gulp.src( path.src + '/styles/main.scss')
        .pipe( config.sourcemaps ? sourcemaps.init() : gutil.noop() )
        .pipe( sass({
          errLogToConsole: true,
          outputStyle: config.minify ? 'compressed' : 'nested',
          precision: 10,
          includePaths: require('node-neat').includePaths
        }))
        .pipe( config.sourcemaps ? sourcemaps.write('.', {
          includeContent: false,
        }) : gutil.noop() )
        .pipe( gulp.dest( path.out + '/styles' ) )
        .pipe( filter('**/*.css') )
        .pipe( reload({stream:true}) );
});

// Images
gulp.task('images', function() {
  return gulp.src( path.src + '/images/**/*' )
        .pipe( imagemin({
          progressive: true,
          interlaced: true,
          svgoPlugins: [{removeUnknownsAndDefaults: false}]
        }))
        .pipe( gulp.dest( path.out + '/images' ) )
        .pipe( reload() );
});

gulp.task('lint', function() {
  return gulp.src( path.src + '/javascripts/**/*.js' )
        .pipe(eslint())
        .pipe(eslint.format());
});

// JS Dev Processing
gulp.task('scripts', ['lint'], function() {
  return gulp.src( path.src + '/javascripts/**/*.js' )
        .pipe( gulp.dest( path.out + '/javascripts' ) )
        .pipe( reload() );
});

// JSPM Dist Bundler
gulp.task('bundle', ['lint'], function( callback ) {
  jspm.setPackagePath('.');
  jspm.bundleSFX(
    path.src + '/javascripts/main',
    path.out + '/javascripts/bundle.js',
    {
      minify: true,
      sourceMaps: true,
      mangle: false,
      buildCSS: false
    }
  ).catch(function( err ) {
    console.log('Bundle Error:', err);
  });
  callback();
});

// HTML Index File
gulp.task('html', function() {
  var env = {};
  env[ environment ] = true;
  return gulp.src( path.src + '/index.html' )
        .pipe( remove( env ) )
        .pipe( replace( '[root]', config.files.assets ) )
        .pipe( config.minify ? htmlmin() : gutil.noop() )
        .pipe( gulp.dest( path.entry ) )
        .pipe( reload() );
});

// Start local express server (dev only)
gulp.task('server', function() {
  server.use( express.static( path.entry ) )
  server.listen( 8000 )
  browserSync({
    proxy: 'localhost:8000',
    open: false,
    notify: false,
    reloadOnRestart: false
  })
});

// Trigger browserSync reload
function reload() {
  if (server) {
    return browserSync.reload({ stream: true });
  }
  return gutil.noop();
}

// Watch Src Files
gulp.task('watch', function() {
  gulp.watch( path.src + '/index.html', ['html']);
  gulp.watch( path.src + '/images/**/*', ['images']);
  gulp.watch( path.src + '/styles/**/*.scss', ['sass']);
  gulp.watch( path.src + '/javascripts/*.js', ['scripts']);
});

// ### Gulp CLI Tasks

// Bundle for Production
gulp.task('dist', function(callback){
  sequence('clean', ['images', 'sass', 'bundle', 'html']);
});

// Dev: Build tasks only
gulp.task('build', function(callback) {
  sequence(['images', 'sass', 'scripts', 'html'], callback );
});

// Dev: Clean, build, watch & serve
gulp.task('default', function(callback) {
  sequence( 'clean', 'build', ['watch', 'server'], callback );
});