## Setup:

Ensure global dependencies:

1. Node https://nodejs.org/
2. Global NPM packages: `npm install -g jspm gulp`
3. Local NPM packages: `npm install`
4. JSPM packages: `jspm install` (automatically run by `npm install`)

## Gulp Tasks:

`gulp`:
> Build, watch & serve at `.`

`gulp dist`:
> Build & bundle jspm modules to `/dist`

`gulp xxx --environment [development, production]`
> Configure any gulp task based on `/config/[development,production].json`.

> Configuration options include paths, flags for minifying, etc.

> All tasks default to `development`.